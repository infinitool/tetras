'   InfiniTool-Tetras CC0 VBA Excel Code Module for similar to C++ MAP() function
'   Used in Survey assessment prototyping in Excel for data validation and indexing metrics.
'   LM: 04/08/2023 beau@bramletts.com

'   NOTE: Not recommended or tested for large spans of integer source values (> 10,000).
'         Not tested taking source values smaller than the target index.
'         Typically just a few hundred possible whole integer values mapping down to a 1-to-10 index.
'
'		  Example:  =MapToIndex( metricVal, -25,100, 1,5)	(Using in XLS cell)
'					? MapToIndex( -6, -25,100, 1,5)			(Using in VBA Immediate Window to test)

'   To use in Excel VBA, Insert a new Code Module into your XLS sheet, paste in the code and save.
'   Save your sheet as an XLSM (Macro Enabled Sheet) so that the function can be called and ran from Excel Cells.
'   Having this code out of the XLS for sharing prevents blocking and false "MACRO SECURITY" errors

Option Explicit
Option Base 0

Public Function MapToIndex(theValue As Integer, valMin As Integer, valMax As Integer, indexMin As Integer, indexMax As Integer, Optional boolBoundToZero = False) As Double
'   Used to map an integer value within a defined range of possible integer values, to a smaller set of index values,
'   and returns the decimal value of where it proportionally fits within the index range.
'
'   SOURCE Range of integer values of SOURCE data (valMin to valMax)            {Ex. -100 to 100 are the range of integers of source data possible totals)
'   TARGET Range of integer indices of the TARGET data (indexMin to indexMax)   (Ex. 1 to 5 are the range of values to map those values to)

'   boundToZero True/False  Controls whether the values passed are constrained to the Upper/Lower limits of the index, or return ZERO (0)

'   Map them linearly proportional to a range of INDEX values.
'   VALUE is the current SOURCE integer value to map to the INDEX               (Ex. -50 => 1.25, +100 => 5)
'   If the passed value is out of the SOURCE floor min/max it returns a ZERO.

    Dim valueList() As Integer
    Dim indexList() As Integer
    Dim ptr         As Integer
    Dim itemCount   As Integer
    Dim valuePtr    As Integer
    Dim tempVal     As Integer
    Dim mapRatio    As Double
    Dim returnVal   As Double
    
    '   Build the Source array to map to index array
    itemCount = valMax - valMin
    ReDim valueList(itemCount)
    tempVal = valMin
    For ptr = 0 To itemCount
        valueList(ptr) = tempVal
        tempVal = tempVal + 1
    Next ptr
    
    '   Find the VALUE position in this SOURCE array list
    valuePtr = 0
    For ptr = 0 To itemCount
        If valueList(ptr) = theValue Then
            valuePtr = ptr
            Exit For
        End If
    Next ptr
	
	'   Calculate (map) the relative fractional/decimal position within the reduced Index value range
    mapRatio = valuePtr / itemCount    
    itemCount = indexMax - indexMin + 1
    returnVal = mapRatio * itemCount
        
    '   What do we do with out of range values that are passed?
    If boolBoundToZero Then
        '   Constrain passed values to the MIN/MAX limits and return 0 if beyond them
        '   Out of range values set to return ZERO
        If returnVal < indexMin Then
            returnVal = 0
        ElseIf returnVal > indexMax Then
            returnVal = 0
        Else
            ' Value is within range
        End If
    Else
        '   Constrain passed values to the MIN/MAX limits and return [theLimits] if beyond them
        If returnVal <= indexMin Then
            returnVal = indexMin
        ElseIf returnVal >= indexMax Then
            returnVal = indexMax
        Else
            ' Value is within range
        End If
    End If
        
    MapToIndex = returnVal
    
End Function

Sub TestCalc()

    Dim ptr As Integer
    For ptr = 25 To 100
    
        'Debug.Print MapToIndex(ptr, 25, 100, 1, 5)
        Debug.Print MapToIndex(ptr, 25, 100, 1, 5, True)
        'Stop
        'Debug.Print MapToIndex(ptr, 30, 95, 1, 5, True)
        'Debug.Print MapToIndex(ptr, 30, 95, 1, 5, False)
            
    Next ptr

End Sub
