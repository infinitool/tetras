The Tetras(tm) "T.E.T.R.A.S. (Technology Estimation of Team Requests for Agile Sequencing)" branding is nod to the Tetris game’s visual UI and how that can help people better understand Product Management and Demand Intake for and by Development Teams.
		
The goal of these materials are to help Agile teams adopt Demand intake methodologies. To incorporate more accurate assessment approaches that are more objective. To jumpstart creating Demand Assessments into ServiceNow's SPM/ITBM tooling.

Assessing Demands empirically with agreed values takes the subjectivity out of estimation and allows competing product teams to self-determine what is the right work to be accepted.

The XLS created by Beau Bramlett allows a team to modify the weights and values for the Size, Who, Why, How.
These values are weighted and map to an index value which is totaled and creates an ROI based on sizing.

You are free to download, use and modify these concepts, examples and templates provided on this GitLab 
site for your personal, non-commercial use.  Copyright infringement begins when people are duplicating these materials and modifying it as if their own creation for payment or financial benefit.
